﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    private float distance = 0;
    private float highscore;
    public Text Highscore;
    public Text pressR;
    public Text pressM;

    void Start()
    {
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
        Highscore.gameObject.SetActive(false);
        highscore = PlayerPrefs.GetFloat("HighScore",0);
        distanceText.text = distance.ToString();
        Highscore.text = "HighScore: " + highscore.ToString("f1");

        pressR.gameObject.SetActive(false);
        pressM.gameObject.SetActive(false);
    }

    void Update()
    {
      
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("f0");
            gameOverText.gameObject.SetActive(true);
            Highscore.gameObject.SetActive(true);

            pressR.text = "Press R to Restart";
            pressM.text = "Press M to go to Menu";
            pressR.gameObject.SetActive(true);
            pressM.gameObject.SetActive(true);
            if (distance > highscore)
            {
                Save();
            }
        }
        else
        {
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("f0");
        }
       
    }
    void Save() {

        PlayerPrefs.SetFloat("HighScore", distance);
        highscore = distance;
        Highscore.text = "HighScore" + highscore.ToString("f1");
        
    }
}

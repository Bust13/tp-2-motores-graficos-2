using UnityEngine;
using UnityEngine.SceneManagement;

public class PressM : MonoBehaviour
{
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Menu : MonoBehaviour
{
    public void Jugar()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void Creditos()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(2);
    }

    public void Volver()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    public void Salir()
    {
        Application.Quit();
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 13;
    private float initialSize;
    private int i = 0;
    private bool floored;
    public Parallax Effects;
    public Image gasolina;
    public float gasolinaTotal;
    public float gasolinaRestante;
    public Text distanceText;
    public Text gameOverText;
    private float distance = 0;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        Effects = FindObjectOfType<Parallax>();
        gasolinaTotal = 30;
        distance = 0;
        distanceText.text = distance.ToString();
    }

    void Update()
    {
        GetInput();
        gasolina.fillAmount = gasolinaRestante / gasolinaTotal;
        gasolinaRestante -= Time.deltaTime;

        if (gasolinaRestante <= 0)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("f0");
            gameOverText.gameObject.SetActive(true);
        }
        else
        {
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("f0");
        }
    }

    private void GetInput()
    {
        Jump();
        Duck();
    }

    private void Jump()
    {
       
        if (Input.GetKeyDown(KeyCode.W))
        {
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
        
    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
            Effects.parallaxEffect = 0;
            Time.timeScale = 0;
           
        }

        if (collision.gameObject.CompareTag("PowerUp"))
        {
            gasolinaRestante = gasolinaTotal;

        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerPowerUps : MonoBehaviour
{
    public static float powerUpVelocity;
    public Image gasolina;
    public Rigidbody rb;
    public float vidaMaxima;
    public float vidarestante;

    void Start()
        {
            rb = GetComponent<Rigidbody>();

        }

        void Update()
        {
            rb.AddForce(new Vector3(-powerUpVelocity, 0, 0), ForceMode.Force);
            OutOfBounds();
        }

        public void OutOfBounds()
        {
            if (this.transform.position.x <= -15)
            {
                Destroy(this.gameObject);
            }
        }
        public void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Jugador"))
            {
            vidarestante = vidaMaxima;
                Destroy(this.gameObject);
            }
        }
    }

